Rendszerfejlesztés II. gyakorlat dokumentáció

Jedis4 csoport

**Csoporttagok:**

Csúri Álmos

Hiezl Róbert

Sárkány Bence

Szabolcs Maár

Lénárt Bence


|Név|Változás|Dátum|Verzió|
| :- | :- | :- | :- |
|Lénárt Bence|Dokumentum letrehozása, #1970-es issue leírása.|2021. 04. 08.|1.0.0|

## Projekt

<https://github.com/redis/jedis>

### Projekt célja

A projekt célja egy olyan open-soruce, memóriában tárolt adatbázis, amelyet egyaránt használhatunk adatbázisként, gyorsítótárként, és message-broker-ként.
Miket tudunk lementeni benne? string, hash, list, set, stored set, range queries, bitmap, hyperloglog, geospatial index, és stream.

### Projekt futtatása

Mivel ez egy kliens alkalmazás, így szükségünk lesz egy szerverre (Redis), ami kiszolgálja a kéréseinket.

1. <https://redis.io/download> letölteni innen a 6.2-es Stable változatot.
1. Kicsomagoljuk.
1. Nyissunk egy Terminal-t az a kicsomagolt mappában.
1. Adjuk ki a következő parancsot: „make”.

![](Aspose.Words.fe048f53-dc8c-4fb1-bc6e-cafeec656bd6.001.png)

Automatikusan generált leírás](Aspose.Words.fe048f53-dc8c-4fb1-bc6e-cafeec656bd6.001.png)

1. Miután végzett, indítsuk el a szervert: „src/redis-server”.

![](Aspose.Words.fe048f53-dc8c-4fb1-bc6e-cafeec656bd6.002.png)

Automatikusan generált leírás](Aspose.Words.fe048f53-dc8c-4fb1-bc6e-cafeec656bd6.002.png)


Van szerverünk, most már csak a projektünket kell futtatni, a kliens alkalmazást. Én Eclipse-t használtam a futtatáshoz. Ehhez csupán annyit kellett csinálnom, hogy hozzáadtam a projekthez egy Main.java osztályt, ami megvalósítja a main() függvényt, vagyis a program belépési pontját.

Még példányosítanom kellett a Jedis osztályt, és így már tudom is futtatni.

![](Aspose.Words.fe048f53-dc8c-4fb1-bc6e-cafeec656bd6.003.png)

Automatikusan generált leírás](Aspose.Words.fe048f53-dc8c-4fb1-bc6e-cafeec656bd6.003.png)

### Feladatok:

- 1439
- 1520
- 1782
- 1799
- 1970
###
### Issues

- <https://github.com/redis/jedis/issues>
- <https://github.com/redis/jedis/issues/1439>
- <https://github.com/redis/jedis/issues/1520>
- <https://github.com/redis/jedis/issues/1799>
- <https://github.com/redis/jedis/issues/1782>
- <https://github.com/redis/jedis/issues/1970>


## Issue #1970

Felelős: Lénárt Bence

*,,Caused by: redis.clients.jedis.exceptions.JedisConnectionException: java.net.SocketException: Connection reset” from Cholf*

### Issue leírása

Az Issue egy kapcsolódási hibát taglal, ami egy le nem kezelt hibát eredményez. Nem minden esetben eredményez hibát, viszont mellékelt beállítást a bejelentő:

*com.jedis.JedisYamlBean { jedisPoolBean: !!com.jedis.JedisPoolBean { maxTotal: 50000, maxIdle: 5000, maxWaitMillis: 3000, testOnBorrow: false, testOnReturn: false, blockWhenExhausted: false, numTestsPerEvictionRun: 100 }, jedisMasterNode: ${redis.master.nodes}, jedisSlaveNode: ${redis.slave.nodes}, timeout: 500, password: ${redis.password}, maxAttempts: 2 }*

### Exception

*java.lang.reflect.InvocationTargetException: null*

*at sun.reflect.GeneratedMethodAccessor102.invoke(Unknown Source)*

*at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)*

*at java.lang.reflect.Method.invoke(Method.java:498)*

*at com.pss.jedis.JedisHandler.lambda$proxyJedis$2(JedisHandler.java:135)*

*at redis.clients.jedis.Jedis$$EnhancerByCGLIB$$d8b7c579.mget()*

*Caused by: redis.clients.jedis.exceptions.JedisConnectionException: java.net.SocketException: Connection reset*

*at redis.clients.util.RedisInputStream.ensureFill(RedisInputStream.java:202)*

*at redis.clients.util.RedisInputStream.readByte(RedisInputStream.java:40)*

*at redis.clients.jedis.Protocol.process(Protocol.java:151)*

*at redis.clients.jedis.Protocol.read(Protocol.java:215)*

*at redis.clients.jedis.Connection.readProtocolWithCheckingBroken(Connection.java:340)*

*at redis.clients.jedis.Connection.getBinaryMultiBulkReply(Connection.java:276)*

*at redis.clients.jedis.Connection.getMultiBulkReply(Connection.java:269)*

*at redis.clients.jedis.Jedis.mget(Jedis.java:407)*

*... 41 common frames omitted*

*Caused by: java.net.SocketException: Connection reset*

*at java.net.SocketInputStream.read(SocketInputStream.java:210)*

*at java.net.SocketInputStream.read(SocketInputStream.java:141)*

*at java.net.SocketInputStream.read(SocketInputStream.java:127)*

*at redis.clients.util.RedisInputStream.ensureFill(RedisInputStream.java:196)*



### Lehetséges megoldás

Egyik kommentben azt írták, hogy nála is előfordult, de ő úgy próbálta meg ezt a hibát megoldani, hogy leellenőrizte az internet kapcsolatot és ha nem volt, akkor megvárta még lett és újra csatlakozott. Illetve, ha kapott egy JedisConnectionException-t, akkor leiratkozott a csatornákról, amikre fel volt iratkozva és vissza kell iratkozni, miután minden rendbe jött. Így először én is ezt fogom tenni. 

Lépések:

1. Ha érzékelem, hogy kaptam egy JedisConnectionException-t, akkor megnézem az internetkapcsolatot.
1. Ha nincs, akkor addig nem fogok semmit csinálni, amíg vissza nem jön.
1. Ha visszajött, akkor az összes feliratkozott csatornára vissza fogok iratkozni.

### Unit test

Meglököm azt a függvényt, ami azt végzi, hogy ne csináljon semmit, várakozzon és meg lököm utána 5000 millisec után azt a függvényt, ami visszaállítja a normál működésbe. Ha nem kapok Exception-t akkor minden rendben van, ha nem, akkor más implementáció szükséges.

### Sequence diagram

A kivétel a connection() során dobódott, ami a lenti ábrán végig lehet követni, hogy milyen funkciók hívódnak meg. Először példányosít egy Socketet, majd ennek a Socket-nek az input és output stream-jét adja át, példányosítván a RedisOutputStream-et és a RedisInputStream-et. Ha a create során exception dobódik, akkor az egy JedisConnectionException lesz. Ha már connectelve van, akkor meghívódik a connect() előtt a disconnect(), és ha a socketParamModified true vagy az isConnected true, akkor terminál a függvény.

![](Aspose.Words.fe048f53-dc8c-4fb1-bc6e-cafeec656bd6.004.png)
### Class diagram

A két érintett classom, a Connection class, ami a redis.clients.jedis.util package-ben található és megvalósítja a Closable-t, illetve a másik, ahol a kivétel is dobódott a RedisOutputStream, ami a redis.clients.jedis-ben található és megvalósítja a FileInputStream interfészt.

![](Aspose.Words.fe048f53-dc8c-4fb1-bc6e-cafeec656bd6.005.png)

